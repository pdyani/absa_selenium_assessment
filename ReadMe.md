### Web assessment
##### Step by step configuration

### Pre-requisite
##### Must Have java 8 installed
##### must have Intellij installed
##### Must have Chrome or Firefox installed

### Dependencies
##### the project is build using Maven dependencies, all required dependencies already included on the project inside pom.xml file

### After Cloning the project
##### Import or open on Intellij
##### You will  notice that the dependencies will start downloading(give it some time for it to complete downloading)

### Three different ways to run the tests
##### Open the test class and click run(execution will start)
##### Open testng.xml, right click and run (execution will start)
### Running on command Prompt
##### open command prompt
##### navigate to the project location using cd then location of your project
##### Enter mvn clean(Make sure the build is success)
##### Enter mvn clean test(execution will start)

### Integrating the project with Jenkins
##### Go to your Jenkins server
### Create a job -enter the following under Build section
##### c: if your project is on drive c Otherwise change accordingly
##### cd then your project location
##### mvn clean test
##### echo Success




















