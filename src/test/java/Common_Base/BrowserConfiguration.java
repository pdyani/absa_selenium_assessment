package Common_Base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserConfiguration {

    public static String dir = System.getProperty("user.dir");

    public static final String Chromedriver = dir+"/Drivers/chromedriver.exe";
    public static final String Firefox = dir+"/Drivers/geckodriver.exe";

    static WebDriver driver;

    public static WebDriver startBrowser(String browserchoice,String url) {
        if ("firefox".equals(browserchoice)) {
        System.setProperty("webdriver.gecko.driver", Firefox);
        driver = new FirefoxDriver();
        } else if ("chrome".equals(browserchoice)) {
            System.setProperty("webdriver.chrome.driver", Chromedriver);
            driver = new ChromeDriver();
        }
         driver.manage().window().maximize();
        driver.get(url);
        return driver;
    }
}
