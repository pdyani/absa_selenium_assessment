package Functionality;

import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbsaWebPageObjects {

    WebDriver driver;
    private Faker nameGenerator = new Faker();
    private String firstName;
    private String lastName;

    public AbsaWebPageObjects(WebDriver driver)
    {
        this.driver=driver;
    }


    // finding elements using Page Factory
    @FindBy(xpath = "//button[@class='btn btn-link pull-right']") WebElement AddUser;

    @FindBy(xpath = "//input[@name='FirstName']") WebElement FirstName;

    @FindBy(xpath = "//input[@name='LastName']") WebElement LastName;

    @FindBy(xpath = "//input[@name='UserName']") WebElement UserName;

    @FindBy(xpath = "//input[@name='Password']") WebElement Password;

    @FindBy(xpath = "//label[@class='radio ng-scope ng-binding'][1]") WebElement CustomerRadioButton;

    @FindBy(xpath = "//select[@name='RoleId']") WebElement RoleID;

    @FindBy(xpath = "//input[@class='ng-pristine ng-valid ng-valid-email']") WebElement Email;

    @FindBy(xpath = "//input[@name='Mobilephone']") WebElement CellPhone;

    @FindBy(xpath = "//button[@class='btn btn-success']") WebElement SaveButton;

    @FindBy(xpath = "/html/body/table/tbody/tr[1]/td[1]") WebElement firstNameGenerated;






    //Click AddUser Link
    public void addUserLink() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(AddUser)).click();
    }

    //    input FirstName in FirstName Field
    public void inputFirstName() {
        firstName = nameGenerator.name().firstName();
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(FirstName)).sendKeys(firstName);
    }

    //    input LastName in LastName Field
    public void inputLastName() {
        lastName = nameGenerator.name().firstName();
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(LastName)).sendKeys(lastName);
    }

    //    Input UserName in UserName Field
    public void inputUserName(String userName) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(UserName)).sendKeys(userName);
    }

    //    Input Password in Password Field
    public void clickPassword(String password) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(Password)).sendKeys(password);
    }

    //    Click to Switch Radio Button
    public void switchRadioButton() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(CustomerRadioButton)).click();
    }


    //    Click RoleID Dopdown Box
    public void clickRoleID() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(RoleID)).click();
    }

    //   Select RoleID item in Dropdown Box
    public void selectRoleID(String Role) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(RoleID)).sendKeys(Role);
    }

    //    input Email in Email Field
    public void inputEmail(String email) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(Email)).sendKeys(email);
    }

    //    Input Cellphone Number in Cell-Phone Field
    public void inputCellPhone(String cellnumber) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(CellPhone)).sendKeys(cellnumber);
    }

    //    Click Save Button to Save User Details
    public void clickSaveButton() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(SaveButton)).click();
    }

    public void checkUserIsAdded() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.textToBePresentInElement(firstNameGenerated,firstName));
    }





}
