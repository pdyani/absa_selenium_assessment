package Tests;

import Common_Base.BrowserConfiguration;
import Common_Base.Report;
import Functionality.AbsaWebPageObjects;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

public class AbsaWebTests extends Report {
    public static String dir = System.getProperty("user.dir");
    public static final String excel = dir+"/TestData/DataSheet.xlsx";

    //To use the different browser change the browser choice, details can be found on BrowserSetup class
    WebDriver driver = BrowserConfiguration.startBrowser("chrome"," http://www.way2automation.com/angularjs-protractor/webtables/");

    AbsaWebPageObjects AbsaWebPage = PageFactory.initElements(driver, AbsaWebPageObjects.class);
    private String filePath;

    @Test(description = "Adding User in User List Table")
    public void CheckingDemoContactMeDetails() throws IOException, InterruptedException {
        test = extent.createTest("Sending Contact Details");
        Date startTime =new Date();
        test.getModel().setStartTime(startTime);
        test.log(Status.INFO, "Checking Demo tests started");
        // the below is the code to read the Contact Us Details
        FileInputStream fis = new FileInputStream(excel);
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        String UserName = sheet.getRow(1).getCell(0).getStringCellValue();
        String Password = sheet.getRow(1).getCell(1).getStringCellValue();
        String Role = sheet.getRow(4).getCell(0).getStringCellValue();
        String Email = sheet.getRow(4).getCell(1).getStringCellValue();
        String Cellphone = sheet.getRow(7).getCell(0).getStringCellValue();



        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "AddUser List Table Page", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());

        AbsaWebPage.addUserLink();
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "AddUser-Link Click Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());


        AbsaWebPage.inputFirstName();
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "FirstName entry in TextBox Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());


        AbsaWebPage.inputLastName();
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "LastName entry in TextBox Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());


        AbsaWebPage.inputUserName(UserName);
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "UserName Entry in TextBox Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());


        AbsaWebPage.clickPassword(Password);
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "Password entry in TextBox Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());


        AbsaWebPage.switchRadioButton();
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "Customer RadioButton Selection Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());


        AbsaWebPage.clickRoleID();
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "RoleID Dropdown Box Click Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());


        AbsaWebPage.selectRoleID(Role);
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "RoleID Selection Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());


        AbsaWebPage.inputEmail(Email);
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "Email entry in Text Box Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());


        AbsaWebPage.inputCellPhone(Cellphone);
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "CellPhone entry in Text Box Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());
        TakeScreeshot(driver);

        AbsaWebPage.clickSaveButton();
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "Save Button Click Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());

        AbsaWebPage.checkUserIsAdded();
        filePath=TakeScreeshot(driver);
        test.log(Status.PASS, "User Added to List Passed", MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());


    }

    public static String TakeScreeshot(WebDriver driver)
    {
        TakesScreenshot ts =(TakesScreenshot) driver;
        File src = ts.getScreenshotAs(OutputType.FILE);

        String path =System.getProperty("user.dir")+"/Screenshots/"+System.currentTimeMillis()+".png";

        File destination = new File(path);

        try {
            FileUtils.copyFile(src,destination);
        }catch (IOException e)
        {
            System.out.println("Capture Failed "+e.getMessage());
        }
        return path;
    }

    @AfterClass
    public void closeBrowser() {
        driver.quit();
    }


}
